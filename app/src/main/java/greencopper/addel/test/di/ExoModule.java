package greencopper.addel.test.di;


import android.content.Context;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ExoModule {


    private Context context;

    public ExoModule(Context context) {
        this.context = context;
    }

    @Provides
    DefaultBandwidthMeter provideDefaultBandwidthMeter() {
        return new DefaultBandwidthMeter();
    }


    @Singleton
    @Provides
    SimpleExoPlayer provideSimpleExoPlayer() {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        return ExoPlayerFactory.newSimpleInstance(context, trackSelector);


    }


}
