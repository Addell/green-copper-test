package greencopper.addel.test.di;


import greencopper.addel.test.ui.auth.AuthActivity;
import greencopper.addel.test.ui.main.MainAct;
import greencopper.addel.test.ui.trackList.TrackListFrag;
import greencopper.addel.test.ui.trackPlayer.TrackPlayerFrag;

public interface AppInjector {

    void inject(AuthActivity activity);
    void inject(MainAct activity);
    void inject(TrackListFrag frag);
    void inject(TrackPlayerFrag frag);

}
