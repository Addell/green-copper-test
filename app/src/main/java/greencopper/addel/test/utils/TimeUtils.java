package greencopper.addel.test.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtils {

    public static String getTime(Long ms) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(ms);
        return new SimpleDateFormat("mm:ss").format(cal.getTime());
    }
}
