package greencopper.addel.test.ui.trackList;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.exoplayer2.SimpleExoPlayer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import greencopper.addel.test.App;
import greencopper.addel.test.R;
import greencopper.addel.test.data.Prefs;
import greencopper.addel.test.model.Image;
import greencopper.addel.test.model.Track;
import greencopper.addel.test.model.UserProfil;
import greencopper.addel.test.model.ViewEvent;
import greencopper.addel.test.network.SpotifyService;
import greencopper.addel.test.ui.common.BaseFragment;
import greencopper.addel.test.ui.home.HomeFrag;
import greencopper.addel.test.ui.home.HomePagerAdapter;
import greencopper.addel.test.ui.main.MainAct;
import greencopper.addel.test.ui.trackPlayer.TrackPlayerFrag;
import greencopper.addel.test.utils.StringUtils;
import greencopper.addel.test.view.AvenirTextView;

public class TrackListFrag extends BaseFragment implements TrackListContract.ViewContract {


    @BindView(R.id.rvTrackList)
    RecyclerView rvTrackList;


    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.sdvImageTrack)
    SimpleDraweeView sdvImageTrack;

    @BindView(R.id.tvTrackTitle)
    AvenirTextView tvTrackTitle;

    @BindView(R.id.tvTrackDescription)
    AvenirTextView tvTrackDescription;

    @BindView(R.id.ivPlayPause)
    ImageView ivPlayPause;


    @OnClick(R.id.ivPlayPause)
    void onClickPlayPause() {

        exoPlayer.setPlayWhenReady(!exoPlayer.getPlayWhenReady());
        updateViews(exoPlayer.getPlayWhenReady());

    }

    @OnClick(R.id.rlCurrentTrack)
    void onClickCurrentTrack() {

        EventBus.getDefault().post(new ViewEvent(HomeFrag.class, "next"));

    }



    @Inject
    Prefs prefs;

    @Inject
    SpotifyService spotifyService;

    @Inject
    SimpleExoPlayer exoPlayer;

    private String TAG = getClass().getSimpleName();

    private TrackListContract.Presenter presenter;
    private TrackListPresenter trackListPresenter;
    private TrackListAdapter storeAdapter;
    private StaggeredGridLayoutManager manager;



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        App.getInjector().inject(this);

        this.trackListPresenter = new TrackListPresenter(getContext(), this, spotifyService);
        this.trackListPresenter.subscribe();


    }


    @Override
    public void iniViews() {

        rvTrackList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                /*if (dy > 0) {
                    // Scroll Down
                    if (newSalePoints.isShown()) {
                        newSalePoints.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!newSalePoints.isShown()) {
                        newSalePoints.show();
                    }
                }*/
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                presenter.iniTrackList();

            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimary,
                R.color.colorAccent);


    }


    @Override
    public void dataToViews(ArrayList<Object> objects) {

        storeAdapter = new TrackListAdapter(getActivity(), this, objects);
        manager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        rvTrackList.setLayoutManager(manager);
        rvTrackList.setAdapter(storeAdapter);
        rvTrackList.setHasFixedSize(true);
        storeAdapter.notifyDataSetChanged();
        rvTrackList.smoothScrollToPosition(0);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }



    @Override
    protected int getFragmentLayout() {
        return R.layout.ui_frag_track_list;
    }



    @Override
    public void setPresenter(TrackListContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void showProgress() {
        refreshLayout.setRefreshing(true);

    }

    @Override
    public void hideProgress() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onError(Throwable var1) {

    }

    @Override
    public void onException(String var1) {

    }

    @Override
    public void saveUserProfil(UserProfil userProfil) {
        prefs.save("userProfil", userProfil);
    }

    @Override
    public void playOrPause(Track track) {

        EventBus.getDefault().post(track);

    }

    @Subscribe
    public void onEvent(Track track) {
        if (track != null) {
            ivPlayPause.setImageResource(R.drawable.pause);

            if (StringUtils.isPresent(track.getName()))
                tvTrackTitle.setText(track.getName());

            tvTrackDescription.setText((StringUtils.isPresent(track.getArtists().get(0).getName()) ? track.getArtists().get(0).getName() : " ") + " · " + (StringUtils.isPresent(track.getAlbum().getName()) ? track.getAlbum().getName() : ""));

            if (track.getAlbum().getImages().size() > 0) {
                Image image = track.getAlbum().getImages().get(0);
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(image.getUrl()))
                        .setResizeOptions(new ResizeOptions(450, 450))
                        .build();
                sdvImageTrack.setController(
                        Fresco.newDraweeControllerBuilder()
                                .setOldController(sdvImageTrack.getController())
                                .setImageRequest(request)
                                .build());
            }


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void updateViews(boolean playing) {

        if (playing) {

            ivPlayPause.setImageResource(R.drawable.pause);

        } else {

            ivPlayPause.setImageResource(R.drawable.play);
            ivPlayPause.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            if (exoPlayer!=null)
            updateViews(exoPlayer.getPlayWhenReady());
        }
    }


}
