package greencopper.addel.test.ui.trackList;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import greencopper.addel.test.model.Track;
import greencopper.addel.test.model.UserProfil;
import greencopper.addel.test.network.SpotifyService;
import greencopper.addel.test.data.Prefs;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrackListPresenter implements TrackListContract.Presenter {
    private Context context;
    private TrackListContract.ViewContract viewContract;
    private SpotifyService spotifyService;


    public TrackListPresenter(Context context, TrackListContract.ViewContract viewContract, SpotifyService spotifyService) {
        this.context = context;
        this.viewContract = viewContract;
        this.spotifyService = spotifyService;
        viewContract.setPresenter(this);
    }

    public void subscribe() {
        this.viewContract.iniViews();
        loadProfil();

    }

    public void unSubscribe() {
    }

    @Override
    public void iniTrackList() {
        spotifyService.getTopTracks()
                .cache()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonObject>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                        viewContract.showProgress();
                    }

                    @Override
                    public void onComplete() {
                        viewContract.hideProgress();
                    }


                    @Override
                    public void onError(final Throwable error) {

                        viewContract.hideProgress();
                    }

                    @Override
                    public void onNext(JsonObject respond) {



                        ArrayList<Track> tracks= (ArrayList<Track>) new Gson().fromJson(respond.getAsJsonArray("items"),
                                new TypeToken<ArrayList<Track>>() {
                                }.getType());


                        viewContract.dataToViews( new ArrayList<Object>(tracks));


                    }


                });
    }


    @Override
    public void loadProfil() {

        spotifyService.getUserProfil()
                .cache()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserProfil>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                        viewContract.showProgress();
                    }

                    @Override
                    public void onComplete() {
                        viewContract.hideProgress();
                    }


                    @Override
                    public void onError(final Throwable error) {
                        viewContract.hideProgress();

                    }

                    @Override
                    public void onNext(UserProfil userProfil) {

                        viewContract.saveUserProfil(userProfil);

                        iniTrackList();

                    }


                });
    }
}
