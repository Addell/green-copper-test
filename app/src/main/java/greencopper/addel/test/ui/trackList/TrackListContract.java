package greencopper.addel.test.ui.trackList;


import java.util.ArrayList;

import greencopper.addel.test.model.Track;
import greencopper.addel.test.model.UserProfil;
import greencopper.addel.test.ui.common.BasePresenter;
import greencopper.addel.test.ui.common.BaseView;

public interface TrackListContract {
    interface Presenter extends BasePresenter {
        void iniTrackList();

        void loadProfil();
    }

    interface ViewContract extends BaseView<Presenter> {


        void dataToViews(ArrayList<Object> objects);

        void saveUserProfil(UserProfil userProfil);

        void playOrPause(Track track);

    }
}

