package greencopper.addel.test.ui.trackPlayer;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.imagepipeline.request.Postprocessor;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import greencopper.addel.test.App;
import greencopper.addel.test.R;
import greencopper.addel.test.model.Image;
import greencopper.addel.test.model.Track;
import greencopper.addel.test.model.ViewEvent;
import greencopper.addel.test.ui.common.BaseFragment;
import greencopper.addel.test.ui.home.HomeFrag;
import greencopper.addel.test.utils.StringUtils;
import greencopper.addel.test.view.AvenirTextView;
import jp.wasabeef.fresco.processors.BlurPostprocessor;

public class TrackPlayerFrag extends BaseFragment implements TrackPlayerContract.ViewContract, ExoPlayer.EventListener {


    @BindView(R.id.sdvImageTrack)
    SimpleDraweeView sdvImageTrack;


    @BindView(R.id.sdvImageTrackBlur)
    SimpleDraweeView sdvImageTrackBlur;

    @BindView(R.id.tvTrackTitle)
    AvenirTextView tvTrackTitle;

    @BindView(R.id.tvTrackDescription)
    AvenirTextView tvTrackDescription;

    @BindView(R.id.ivPlayPause)
    ImageView ivPlayPause;


    @BindView(R.id.sbTrackProgress)
    SeekBar sbTrackProgress;


    @OnClick(R.id.ivBack)
    void onClickBack() {
        EventBus.getDefault().post(new ViewEvent(HomeFrag.class, "prev"));
    }

    @OnClick(R.id.ivPlayPause)
    void onClickPlayPause() {

        exoPlayer.setPlayWhenReady(!exoPlayer.getPlayWhenReady());
        updateViews(exoPlayer.getPlayWhenReady());

    }

    @OnClick(R.id.ivSkip)
    void onClickSkip() {
        stop();
        onClickPlayPause();
    }

    @Inject
    SimpleExoPlayer exoPlayer;


    public static final String TAG = TrackPlayerFrag.class.getSimpleName();
    private final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgressBar();
        }
    };

    private TrackPlayerContract.Presenter presenter;
    private TrackPlayerPresenter trackPlayerPresenter;
    private Handler handler;
    private Track track;

    private Postprocessor postprocessor;
    private ImageRequest imageRequest;
    private PipelineDraweeController controller;
    private int BLUR_PRECENTAGE = 50;

    @Override
    protected int getFragmentLayout() {
        return R.layout.ui_frag_track_player;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        App.getInjector().inject(this);

        this.trackPlayerPresenter = new TrackPlayerPresenter(getContext(), this);
        this.trackPlayerPresenter.subscribe();

        exoPlayer.addListener(this);
        updateViews(exoPlayer.getPlayWhenReady());


        postprocessor = new BlurPostprocessor(getActivity(), BLUR_PRECENTAGE);


    }

    @Override
    public void iniViews() {


    }

    @Override
    public void iniSbTrackBar() {
        sbTrackProgress.setPadding(8, 0, 8, 0);
        sbTrackProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (fromUser) {
                    long seekPosition = exoPlayer.getDuration() == -1 ? 0
                            : Math.min(Math.max(0, progress), exoPlayer.getDuration());
                    exoPlayer.seekTo(seekPosition);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void updateProgressBar() {
        long duration = exoPlayer == null ? 0 : exoPlayer.getDuration();
        long position = exoPlayer == null ? 0 : exoPlayer.getCurrentPosition();

        if (sbTrackProgress != null) {
            sbTrackProgress.setMax(Long.valueOf(duration).intValue());
            sbTrackProgress.setProgress(Long.valueOf(position).intValue());

        }

        handler = new Handler();
        handler.removeCallbacks(updateProgressAction);

        int playbackState = exoPlayer == null ? exoPlayer.STATE_IDLE : exoPlayer.getPlaybackState();
        if (playbackState != ExoPlayer.STATE_IDLE && playbackState != ExoPlayer.STATE_ENDED) {
            long delayMs;
            if (exoPlayer.getPlayWhenReady() && playbackState == ExoPlayer.STATE_READY) {
                delayMs = 50 - (position % 50);
                if (delayMs < 50) {
                    delayMs += 50;
                }
            } else {
                delayMs = 50;
            }
            handler.postDelayed(updateProgressAction, delayMs);
        }
    }

    public void play(String streamUrl) {

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(getContext(), getUserAgent(), BANDWIDTH_METER);
        ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .setExtractorsFactory(new DefaultExtractorsFactory())
                .createMediaSource(Uri.parse(streamUrl));
        exoPlayer.prepare(mediaSource);
        exoPlayer.setPlayWhenReady(true);


    }


    public void pause() {
        exoPlayer.setPlayWhenReady(false);
    }

    public void stop() {
        exoPlayer.seekTo(0, 0);

    }

    private void releasePlayer() {
        exoPlayer.stop();
        exoPlayer.release();
        exoPlayer = null;
    }


    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        updateProgressBar();
    }


    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }


    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onSeekProcessed() {

    }


    private String getUserAgent() {
        return Util.getUserAgent(getContext(), getClass().getSimpleName());
    }


    @Subscribe
    public void onEvent(Track track) {
        if (track != null) {


            ivPlayPause.setImageResource(R.drawable.pause);

            if (StringUtils.isPresent(track.getName()))
                tvTrackTitle.setText(track.getName());

            tvTrackDescription.setText((StringUtils.isPresent(track.getArtists().get(0).getName()) ? track.getArtists().get(0).getName() : " ") + " · " + (StringUtils.isPresent(track.getAlbum().getName()) ? track.getAlbum().getName() : ""));

            if (track.getAlbum().getImages().size() > 0) {
                Image image = track.getAlbum().getImages().get(0);
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(image.getUrl()))
                        .setResizeOptions(new ResizeOptions(450, 450))
                        .build();
                sdvImageTrack.setController(
                        Fresco.newDraweeControllerBuilder()
                                .setOldController(sdvImageTrack.getController())
                                .setImageRequest(request)
                                .build());


                imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(image.getUrl()))
                        .setPostprocessor(postprocessor)
                        .build();
                controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                        .setImageRequest(imageRequest)
                        .setOldController(sdvImageTrackBlur.getController())
                        .build();
                sdvImageTrackBlur.setController(controller);


            }

            this.track = track;

            if (StringUtils.isPresent(this.track.getPreviewUrl()))
                play(this.track.getPreviewUrl());

        }
    }


    @Override
    public void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);

    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }


    private void updateViews(boolean playing) {

        if (playing) {

            ivPlayPause.setImageResource(R.drawable.pause);

        } else {

            ivPlayPause.setImageResource(R.drawable.play);
            ivPlayPause.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

        }

    }

    @Override
    public void setPresenter(TrackPlayerContract.Presenter var1) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onError(Throwable var1) {

    }

    @Override
    public void onException(String var1) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (exoPlayer != null)
                updateViews(exoPlayer.getPlayWhenReady());
        }
    }

}