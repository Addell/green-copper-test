package greencopper.addel.test.ui.trackPlayer;


import java.util.ArrayList;

import greencopper.addel.test.ui.common.BasePresenter;
import greencopper.addel.test.ui.common.BaseView;

public interface TrackPlayerContract {
     interface Presenter extends BasePresenter {

    }

     interface ViewContract extends BaseView<Presenter> {
         void iniSbTrackBar();

    }
}

