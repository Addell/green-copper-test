package greencopper.addel.test.ui.common;

public interface BaseView<T> {
    void iniViews();
    void setPresenter(T var1);

    void showProgress();

    void hideProgress();

    void onError(Throwable var1);

    void onException(String var1);
}
