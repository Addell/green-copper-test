package greencopper.addel.test.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import greencopper.addel.test.App;
import greencopper.addel.test.R;
import greencopper.addel.test.ui.common.BaseActivity;
import greencopper.addel.test.ui.home.HomeFrag;

public class MainAct extends BaseActivity {

    @BindView(R.id.rlDrawer)
    RelativeLayout rlDrawer;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;


    public static final String TAG = MainAct.class.getSimpleName();


    private Fragment fragment = null;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private android.support.v4.app.FragmentTransaction fragmentTransaction;


    public void popFragment() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void iniFragment(int id, Fragment fragment, Boolean tBK) {

        this.fragment = fragment;
        Fragment current = getSupportFragmentManager().findFragmentById(R.id.flContainerFrag);

        //if (current == null || !current.getClass().equals(fragment.getClass()))
        if (fragment != null) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);

            if (tBK)
                fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commit();
            getSupportFragmentManager().executePendingTransactions();
        }
    }

    public void Open(View view) {
        drawerLayout.openDrawer(rlDrawer);
    }

    public void Close(View view) {
        drawerLayout.closeDrawer(rlDrawer);
    }


    @Override
    public void setDataToView(Bundle var1) {

        App.getInjector().inject(this);


        HomeFrag homeFrag = new HomeFrag();
        iniFragment(R.id.flContainerFrag, homeFrag, false);


    }


    @Override
    public int getLayout() {
        return R.layout.ui_act_main;
    }
}