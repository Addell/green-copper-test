package greencopper.addel.test.ui.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import greencopper.addel.test.ui.trackList.TrackListFrag;
import greencopper.addel.test.ui.trackPlayer.TrackPlayerFrag;

public  class HomePagerAdapter extends FragmentPagerAdapter {
        public static int NUM_ITEMS = 2;

        private TrackPlayerFrag trackPlayerFrag;
        private TrackListFrag trackListFrag;

        public HomePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);

            trackPlayerFrag = new TrackPlayerFrag();
            trackListFrag = new TrackListFrag();

        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return trackListFrag;
                case 1:
                    return trackPlayerFrag;
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }