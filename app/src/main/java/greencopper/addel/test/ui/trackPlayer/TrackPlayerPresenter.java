package greencopper.addel.test.ui.trackPlayer;

import android.content.Context;

public class TrackPlayerPresenter implements TrackPlayerContract.Presenter {
    private Context context;
    private TrackPlayerContract.ViewContract viewContract;

    public TrackPlayerPresenter(Context context, TrackPlayerContract.ViewContract viewContract) {
        this.context = context;
        this.viewContract=viewContract;
        this.viewContract.setPresenter(this);
    }

    @Override
    public void subscribe() {

        viewContract.iniSbTrackBar();
    }

    @Override
    public void unSubscribe() {

    }
}
