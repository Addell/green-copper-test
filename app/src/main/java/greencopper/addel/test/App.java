package greencopper.addel.test;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.stetho.Stetho;

import greencopper.addel.test.di.ApiModule;
import greencopper.addel.test.di.AppInjector;
import greencopper.addel.test.di.AppModule;
import greencopper.addel.test.di.DaggerAppComponent;
import greencopper.addel.test.di.ExoModule;


public class App extends Application {
    private static App sInstance;

    private Typeface avenirFont, avenirBoldFont;
    private Context context;
    private static AppInjector injector;

    public static void initialize(Context applicationContext) {
        injector = DaggerAppComponent.builder()
                .appModule(new AppModule(applicationContext))
                .apiModule(new ApiModule())
                .exoModule(new ExoModule(applicationContext))
                .build();
    }

    public static AppInjector getInjector() {
        return injector;
    }

    public static synchronized App getInstance() {
        return sInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        context = getApplicationContext();

        initialize(getApplicationContext());

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(context)
                .setDownsampleEnabled(true)
                .build();
        Stetho.initializeWithDefaults(this);

        Fresco.initialize(this, config);
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearMemoryCaches();
        imagePipeline.clearDiskCaches();
        imagePipeline.clearCaches();

        extractFonts();


    }

    public void extractFonts() {
        avenirFont = Typeface.createFromAsset(this.getAssets(), "Avenir.ttf");
        avenirBoldFont = Typeface.createFromAsset(this.getAssets(), "AvenirBold.ttf");

    }


    public Typeface getTypeface() {

        return avenirFont;
    }

    public Typeface getTypeface(String font) {

        switch (font) {


            case "Avenir":
                return avenirFont;
            case "AvenirBold":
                return avenirBoldFont;

            default:
                return avenirFont;

        }
    }


    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}