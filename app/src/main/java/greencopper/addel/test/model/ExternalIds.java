
package greencopper.addel.test.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class ExternalIds {

    @Expose
    private String isrc;

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

}
