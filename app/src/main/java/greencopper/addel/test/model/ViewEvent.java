package greencopper.addel.test.model;

/**
 * Created by mac on 19/02/2018.
 */

public class ViewEvent {
    private Class aClass;
    private String operation;


    public ViewEvent() {
    }

    public ViewEvent(Class aClass, String operation) {
        this.aClass = aClass;
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Class getaClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }
}
